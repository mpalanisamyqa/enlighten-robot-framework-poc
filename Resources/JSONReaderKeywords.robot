*** Keywords ***
get data from user json
    [Arguments]    ${env}  ${usertype}  ${key}
    ${data_as_string} =    Get File    ${file_path}
    ${data_as_json} =    json.loads    ${data_as_string}
    # looking into the dict at ["d"]["a"] will return the list
    [Return]    ${data_as_json["${env}"]["${usertype}"]["${key}"]}


*** Variables ***
${file_path}  /Users/mnpalanisamy/PycharmProjects/EnlightenRobotFWPOC/TestDatas/Users.json
