*** Settings ***
Variables   ../PageObjects/SignInLocators.py


*** Keywords ***
Login to application
    [Arguments]  ${username}   ${password}
    Input Text  ${email_txt_field}  ${username}
    Input Text  ${password_txt_field}  ${password}
    Click Element   ${submit_btn}
    sleep  5

