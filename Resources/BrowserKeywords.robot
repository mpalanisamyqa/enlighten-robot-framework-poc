#This robot file constains only hooks activities of browser
*** Keywords ***

Start Browser
    [Arguments]  ${url}  ${browser}
    Open Browser  ${url}  ${browser}
    Maximize Browser Window

Terminate Browser
    Close Browser

Terminate All Browser
    Close All Browser