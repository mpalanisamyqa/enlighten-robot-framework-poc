*** Settings ***
Documentation    Suite description
Library  SeleniumLibrary
Library  OperatingSystem
Library  json
Resource  ../Resources/BrowserKeywords.robot
Resource  ../Resources/JSONReaderKeywords.robot
Resource  ../Resources/SignInKeywords.robot

Test Setup  Start Browser  ${URL}   ${Browser}
Test Teardown  Terminate Browser

*** Variables ***
${URL}  %{url}
${Browser}  %{browser}

*** Test Cases ***
Login as Installer
    [Tags]    sanity  installer_sanity

    #Get both email and password from JSON file
    ${email}  get data from user json  qa2  install_admin  email
    ${password}  get data from user json  qa2  install_admin  password
    Login to application  ${email}  ${password}
    Page should contain element  xpath://span[text()='add a widget']

Logout as Installer
    [Tags]   sanity  installer_sanity
    Login as Installer
    Logout from application
    Page should contain element



